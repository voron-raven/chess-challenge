"""
Usage: chess.py.

Class Board for Chess Challenge mini-project.
"""

from __future__ import print_function
import chess_rules
import copy


class ChessError(Exception):
    """Chess Exception, e.g. 'Incorrect position', 'under attack'."""

    def __init__(self, arg):
        """Create a custom Error."""
        super(ChessError, self).__init__(arg)
        self.args = arg


class Piece(object):
    """Represents Chess Piece."""

    def __init__(self, name, board_size, cell):
        """Create a piece on given position."""
        # Validation by name.
        if name not in chess_rules.RULES:
            raise ChessError('Not valid piece name.')

        # Validation by position.
        if (cell[0] < 0 or
                cell[0] >= board_size[0] or
                cell[1] < 0 or
                cell[1] >= board_size[1]):
            raise ChessError('Incorrect position for ' + name)

        self.name = name
        self.row = cell[0]
        self.col = cell[1]
        self.board_height = board_size[0]
        self.board_width = board_size[1]

        # Initialized empty board.
        self._cells = []
        for row_index in range(self.board_height):
            self._cells.append([])
            for _ in range(self.board_width):
                self._cells[row_index].append(chess_rules.EMPTY)

        self._under_attack = []

        # Place the piece.
        self._cells[cell[0]][cell[1]] = self.name

        # Set all available for attack position to UNDER_ATTACK.
        for available_move in chess_rules.RULES[name]:
            # Check if the piece can move by line.
            line_move = len(available_move) > 2
            # Cell to check if it's under attack.
            move = {
                'row': cell[0] + available_move[0],
                'col': cell[1] + available_move[1],
            }

            # While the cell is on board.
            while (0 <= move['row'] < board_size[0] and
                    0 <= move['col'] < board_size[1]):
                self._cells[move['row']][move['col']] = chess_rules.UNDER_ATTACK
                self._under_attack.append((move['row'], move['col']))

                # If the piece can't  move by line - break.
                if not line_move:
                    break

                # Set next cell for checking.
                move['row'] += available_move[0]
                move['col'] += available_move[1]

    def under_attack_cells(self):
        """Return cell under attack."""
        return self._under_attack

    def board(self):
        """Return a board with given piece."""
        return self._cells


class Board(object):
    """Represents Chess board."""

    def __init__(self, height, width, pieces=None):
        """Create a board of given size and pieces."""
        self._height = int(height)
        self._width = int(width)
        self._variants = 0

        # Setup matrix with available cells.
        self._cells = []
        for row_index in range(self._height):
            self._cells.append([])
            for _ in range(self._width):
                self._cells[row_index].append(chess_rules.EMPTY)

        # Setup a list with pieces.
        self._pieces = []

        # We need to use double sorting for pieces.
        # Fist put pieces with known position and most pageful.
        if pieces:
            self.__pieces_list = pieces

            for piece in self.__pieces_list:
                if len(piece) > 1:
                    self.add_piece(piece)

    def __str__(self):
        """Return string representation of chess table."""
        string_table = ''
        for row in reversed(self._cells):
            string_table += '\n' + ' '.join([str(cell) for cell in row])
        return string_table

    def board(self):
        """Return a board."""
        return tuple(tuple(row) for row in self._cells)

    def is_available(self, board_row, board_col):
        """Return True if cell is available."""
        return self._cells[board_row][board_col] == chess_rules.EMPTY

    def available_cells(self):
        """Return all available cells."""
        available_cells = []
        for row in range(self._height):
            for col in range(self._width):
                if self.is_available(row, col):
                    available_cells.append((row, col))
        return available_cells

    def add_piece(self, piece):
        """Add piece to the board."""
        if not self.is_available(piece[1], piece[2]):
            raise_text = 'Incorrect position for ' +\
                         piece[0] +\
                         '. Cell is under attack.'
            raise ChessError(raise_text)

        pi_obj = Piece(
            piece[0],
            (self._height, self._width),
            (piece[1], piece[2])
        )
        under_attack_cells = pi_obj.under_attack_cells()

        if len(set(self.pieces()) & set(under_attack_cells)) == 0:
            self._pieces.append((pi_obj.row, pi_obj.col))
            # Number which will represent the piece.
            self._cells[pi_obj.row][pi_obj.col] = piece[0]
            for cell in under_attack_cells:
                self._cells[cell[0]][cell[1]] = chess_rules.UNDER_ATTACK

        else:
            raise_text = 'Incorrect position for ' +\
                         pi_obj.name +\
                         '. Existing piece will be under attack.'
            raise ChessError(raise_text)

    def pieces(self):
        """Return array of coordinates for pieces."""
        return self._pieces

    def find_place_for_pieces(self, pieces=None, silent=False):
        """Return array of coordinates where pieces could be placed."""
        _solutions = set()

        def find_place(chessmen, available_cells, board, cur_result):
            """Nested function with recursion to find pieces position."""
            if not chessmen:
                _solutions.add(board.board())
                return cur_result
            else:
                new_pieces = chessmen[:]
                piece = new_pieces.pop(0)
                for cell in available_cells:
                    try:
                        _temp_board = copy.deepcopy(board)
                        _temp_board.add_piece([piece, cell[0], cell[1]])
                        new_available_cells = _temp_board.available_cells()
                        new_result = cur_result[:]
                        new_result.append(cell)
                        find_place(
                            new_pieces,
                            new_available_cells,
                            _temp_board,
                            new_result
                        )
                    except ChessError:
                        # Existing piece was under attack.
                        continue

        if not pieces:
            return 0
        else:
            # Place most powerful pieces on the beginning for optimization.
            pieces = sorted(pieces, key=list(chess_rules.RULES).index)
            find_place(pieces, self.available_cells(), self, [])

            # Print solutions if silent mode is disabled.
            if not silent:
                for solution in _solutions:
                    print()
                    for row in solution:
                        print(' '.join([str(cell) for cell in row]))

            return len(_solutions)
