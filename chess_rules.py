"""
Usage: piece.py, board.py.

Global constants for Chess Challenge mini-project.
"""

EMPTY = 0
UNDER_ATTACK = 1

# Cells available for move.
# Most powerful pieces on the beginning (it matters for optimization).
# Format: ((offset by height, offset by wight, True -
# mean not only nearest cell, but all line is available for move)).
RULES = {
    # Queen.
    'Q': (
        (1, 0, True),
        (1, 1, True),
        (0, 1, True),
        (-1, 1, True),
        (-1, 0, True),
        (-1, -1, True),
        (0, -1, True),
        (1, -1, True),
    ),
    # Rook.
    'R': (
        (1, 0, True),
        (0, 1, True),
        (-1, 0, True),
        (0, -1, True),
    ),
    # Bishop.
    'B': (
        (1, 1, True),
        (-1, 1, True),
        (-1, -1, True),
        (1, -1, True),
    ),
    # King.
    'K': (
        (1, 0),
        (1, 1),
        (0, 1),
        (-1, 1),
        (-1, 0),
        (-1, -1),
        (0, -1),
        (1, -1),
    ),
    # Knight.
    'N': (
        (2, 1),
        (1, 2),
        (-1, 2),
        (-2, 1),
        (-2, -1),
        (-1, -2),
        (1, -2),
        (2, -1),
    ),
}
