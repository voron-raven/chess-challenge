"""Chess tests."""

import os
import sys
import unittest
sys.path.insert(0, os.path.abspath('..'))

from chess.models import Board, Piece, ChessError


class PieceTests(unittest.TestCase):
    """Piece tests."""

    def test_initialize_piece(self):
        """Check piece initialization.."""
        test_map = (
            (
                Piece('Q', (4, 4), (2, 2)),
                [
                    [1, 0, 1, 0],
                    [0, 1, 1, 1],
                    [1, 1, 'Q', 1],
                    [0, 1, 1, 1]
                ]
            ),
            (
                Piece('R', (4, 4), (2, 2)),
                [
                    [0, 0, 1, 0],
                    [0, 0, 1, 0],
                    [1, 1, 'R', 1],
                    [0, 0, 1, 0]
                ]
            ),
            (
                Piece('B', (4, 4), (2, 2)),
                [
                    [1, 0, 0, 0],
                    [0, 1, 0, 1],
                    [0, 0, 'B', 0],
                    [0, 1, 0, 1]
                ]
            ),
            (
                Piece('K', (4, 4), (2, 2)),
                [
                    [0, 0, 0, 0],
                    [0, 1, 1, 1],
                    [0, 1, 'K', 1],
                    [0, 1, 1, 1]
                ]
            ),
            (
                Piece('N', (4, 4), (2, 2)),
                [
                    [0, 1, 0, 1],
                    [1, 0, 0, 0],
                    [0, 0, 'N', 0],
                    [1, 0, 0, 0]
                ]
            )
        )
        for test_case in test_map:
            self.assertEqual(test_case[0].board(), test_case[1])

    def test_under_attack_cells(self):
        """Check if cells under attack for given piece are correct."""
        test_map = (
            (
                Piece('Q', (4, 4), (2, 2)),
                [
                    (3, 2),
                    (3, 3),
                    (2, 3),
                    (1, 3),
                    (1, 2),
                    (0, 2),
                    (1, 1),
                    (0, 0),
                    (2, 1),
                    (2, 0),
                    (3, 1)
                ]
            ),
            (
                Piece('R', (4, 4), (2, 2)),
                [
                    (3, 2),
                    (2, 3),
                    (1, 2),
                    (0, 2),
                    (2, 1),
                    (2, 0)
                ]
            ),
            (
                Piece('B', (4, 4), (2, 2)),
                [
                    (3, 3),
                    (1, 3),
                    (1, 1),
                    (0, 0),
                    (3, 1)
                ]
            ),
            (
                Piece('K', (4, 4), (2, 2)),
                [
                    (3, 2),
                    (3, 3),
                    (2, 3),
                    (1, 3),
                    (1, 2),
                    (1, 1),
                    (2, 1),
                    (3, 1)
                ]
            ),
            (
                Piece('N', (4, 4), (2, 2)),
                [
                    (0, 3),
                    (0, 1),
                    (1, 0),
                    (3, 0)
                ]
            )
        )
        for test_case in test_map:
            self.assertEqual(test_case[0].under_attack_cells(), test_case[1])


class BoardTests(unittest.TestCase):
    """Board tests."""

    def test_initialize_board(self):
        """Check board initialization."""
        test_board = Board(4, 4).board()
        should_be = (
            (0, 0, 0, 0),
            (0, 0, 0, 0),
            (0, 0, 0, 0),
            (0, 0, 0, 0)
        )
        self.assertEqual(test_board, should_be)

    def test_is_available(self):
        """Check if cell is available."""
        # Check for a Queen.
        test_board = Board(4, 4, [['Q', 2, 2]])
        self.assertEqual(test_board.is_available(2, 2), False)
        self.assertEqual(test_board.is_available(3, 3), False)
        self.assertEqual(test_board.is_available(1, 0), True)
        self.assertEqual(test_board.is_available(0, 3), True)

        # Check for a Rook.
        test_board = Board(4, 4, [['R', 2, 2]])
        self.assertEqual(test_board.is_available(2, 2), False)
        self.assertEqual(test_board.is_available(3, 3), True)
        self.assertEqual(test_board.is_available(2, 0), False)
        self.assertEqual(test_board.is_available(0, 3), True)

        # Check for a Bishop.
        test_board = Board(4, 4, [['B', 2, 2]])
        self.assertEqual(test_board.is_available(2, 2), False)
        self.assertEqual(test_board.is_available(3, 3), False)
        self.assertEqual(test_board.is_available(2, 0), True)
        self.assertEqual(test_board.is_available(0, 3), True)

        # Check for a King.
        test_board = Board(4, 4, [['K', 2, 2]])
        self.assertEqual(test_board.is_available(2, 2), False)
        self.assertEqual(test_board.is_available(3, 3), False)
        self.assertEqual(test_board.is_available(2, 0), True)
        self.assertEqual(test_board.is_available(0, 3), True)

        # Check for a Knight.
        test_board = Board(4, 4, [['N', 2, 2]])
        self.assertEqual(test_board.is_available(2, 2), False)
        self.assertEqual(test_board.is_available(3, 3), True)
        self.assertEqual(test_board.is_available(2, 0), True)
        self.assertEqual(test_board.is_available(0, 3), False)

    def test_available_cells(self):
        """Check if available cells are correct."""
        test_map = (
            (
                Board(4, 4, [['Q', 2, 2]]),
                [
                    (0, 1),
                    (0, 3),
                    (1, 0),
                    (3, 0)
                ]
            ),
            (
                Board(4, 4, [['R', 2, 2]]),
                [
                    (0, 0),
                    (0, 1),
                    (0, 3),
                    (1, 0),
                    (1, 1),
                    (1, 3),
                    (3, 0),
                    (3, 1),
                    (3, 3)
                ]
            ),
            (
                Board(4, 4, [['B', 2, 2]]),
                [
                    (0, 1),
                    (0, 2),
                    (0, 3),
                    (1, 0),
                    (1, 2),
                    (2, 0),
                    (2, 1),
                    (2, 3),
                    (3, 0),
                    (3, 2)
                ]
            ),
            (
                Board(4, 4, [['K', 2, 2]]),
                [
                    (0, 0),
                    (0, 1),
                    (0, 2),
                    (0, 3),
                    (1, 0),
                    (2, 0),
                    (3, 0)
                ]
            ),
            (
                Board(4, 4, [['N', 2, 2]]),
                [
                    (0, 0),
                    (0, 2),
                    (1, 1),
                    (1, 2),
                    (1, 3),
                    (2, 0),
                    (2, 1),
                    (2, 3),
                    (3, 1),
                    (3, 2),
                    (3, 3)
                ]
            )
        )
        for test_case in test_map:
            self.assertEqual(test_case[0].available_cells(), test_case[1])

    def test_add_piece(self):
        """Check add_piece method."""
        test_map = (
            (
                ('Q', 2, 2),
                False
            ),
            (
                ('R', 2, 2),
                (
                    ('R', 1, 1, 1),
                    (1, 0, 1, 0),
                    (1, 1, 'R', 1),
                    (1, 0, 1, 0)
                )
            ),
            (
                ('B', 2, 2),
                False
            ),
            (
                ('K', 2, 2),
                (
                    ('R', 1, 1, 1),
                    (1, 1, 1, 1),
                    (1, 1, 'K', 1),
                    (1, 1, 1, 1)
                )
            ),
            (
                ('N', 2, 2),
                (
                    ('R', 1, 1, 1),
                    (1, 0, 0, 0),
                    (1, 0, 'N', 0),
                    (1, 0, 0, 0)
                )
            )
        )
        for piece, should_be in test_map:
            test_board = Board(4, 4, [['R', 0, 0]])
            try:
                test_board.add_piece(piece)
                self.assertEqual(test_board.board(), should_be)
            except ChessError:
                self.assertEqual(False, should_be)

    def test_pieces(self):
        """Check if returned pieces are correct."""
        test_map = (
            (
                ('Q', 2, 2),
                False
            ),
            (
                ('R', 1, 2),
                [
                    (0, 0),
                    (1, 2)
                ]
            ),
            (
                ('B', 2, 2),
                False
            ),
            (
                ('K', 2, 3),
                [
                    (0, 0),
                    (2, 3)
                ]
            ),
            (
                ('N', 2, 1),
                False
            )
        )
        for piece, should_be in test_map:
            test_board = Board(4, 4, [['R', 0, 0]])
            try:
                test_board.add_piece(piece)
                self.assertEqual(test_board.pieces(), should_be)
            except ChessError:
                self.assertEqual(False, should_be)

    def test_find_place_for_pieces(self):
        """Check find_place_for_pieces method."""
        test_board = Board(2, 2)
        self.assertEqual(
            test_board.find_place_for_pieces(
                ['K'], True
            ),
            4
        )

        test_board = Board(5, 5)
        self.assertEqual(
            test_board.find_place_for_pieces(
                ['Q', 'Q', 'Q', 'Q', 'Q'], True
            ),
            10
        )

        test_board = Board(3, 3)
        self.assertEqual(
            test_board.find_place_for_pieces(
                ['K', 'K', 'R'], True
            ),
            4
        )

        test_board = Board(4, 4)
        self.assertEqual(
            test_board.find_place_for_pieces(
                ['N', 'N', 'N', 'N', 'R', 'R'], True
            ),
            8
        )


if __name__ == '__main__':
    unittest.main()
