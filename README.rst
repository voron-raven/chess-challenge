----------------------------
Chess Challenge mini-project
----------------------------

Find all unique configurations of a set of normal chess pieces on a chess board with dimensions M×N where none of the pieces is in a position to take any of the others.

-------
Running
-------

To run program:

    python3 .

Possible positions will be shown (if silent mode is disabled), in the end number of combination will be shown.

Example:

.. image:: https://drive.google.com/uc?export=download&id=0B82PElAuKQS_VGpsWnp0eDBCX0E

-------
Testing
-------

To run tests:

    python3 -m tests.tests

---------------------
Technical description
---------------------

Python 2/3 compatible

Pylint score 10/10

8 unittests