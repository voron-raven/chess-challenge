"""Python - Chess Challenge mini-project."""

from __future__ import print_function
from timeit import timeit
import chess_rules
from models import Board


def main():
    """Entry point if called as an executable."""
    # Trick to make input Python 2 compatible.
    try:
        get_input = raw_input
    except NameError:
        get_input = input

    # Program description.
    print('Welcome to Chess solver!')
    print('This program will find a number of possible'
          ' positions for given pieces on given board.')

    # Get board size.
    print()
    board_height = int(get_input('Please provide a board height in cells: '))
    board_width = int(get_input('Please provide a board width in cells: '))

    # Available pieces.
    print()
    print('Please provide pieces.')
    print('Next pieces are available: ' + ', '.join(list(chess_rules.RULES)))
    print('Type Enter without providing piece to finish.')

    # Get pieces.
    print()
    piece = 'dummy'
    pieces = []
    while piece:
        piece = get_input('Please provide a piece: ').upper()
        assert isinstance(piece, str)
        if piece in list(chess_rules.RULES):
            pieces.append(piece)
        elif piece:
            print('Incorrect piece, please try again.')

    # Should silent mode be deactivated.
    print()
    silent_mode = get_input(
        'Would you like to see combinations '
        '(will be shown in the end)? [No/Yes]: '
    ).upper()
    silent_mode = silent_mode not in ['Y', 'YES']

    # Start calculation.
    print()
    print('Calculation started, please wait.')
    new_board = Board(board_height, board_width)
    result = new_board.find_place_for_pieces(
                pieces,
                silent_mode
            )

    # Show result.
    print()
    print('Done.')
    print('Number of combinations: ' + str(result))


if __name__ == '__main__':
    print(timeit(main, number=1), 's')
